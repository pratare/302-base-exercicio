FROM openjdk:8-alpine
WORKDIR /app
COPY target/Api-Investimentos*.jar ./investimentos.jar
CMD ["java", "-jar", "investimentos.jar"]
